﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class Wall
    {
        public class Item
        {
            public class Attachment
            {
                public string type { get; set; }
                public Photo photo { get; set; }
            }

            public class PostSource
            {
                public string type { get; set; }
                public string platform { get; set; }
            }

            public class Comments
            {
                public int count { get; set; }
                public int can_post { get; set; }
                public bool groups_can_post { get; set; }
            }

            public class Likes
            {
                public int count { get; set; }
                public int user_likes { get; set; }
                public int can_like { get; set; }
                public int can_publish { get; set; }
            }

            public class Reposts
            {
                public int count { get; set; }
                public int user_reposted { get; set; }
            }

            public class Views
            {
                public int count { get; set; }
            }

            public int id { get; set; }
            public int from_id { get; set; }
            public int owner_id { get; set; }
            public int date { get; set; }
            public int marked_as_ads { get; set; }
            public string post_type { get; set; }
            public string text { get; set; }
            public int is_pinned { get; set; }
            public List<Attachment> attachments { get; set; }
            public PostSource post_source { get; set; }
            public Comments comments { get; set; }
            public Likes likes { get; set; }
            public Reposts reposts { get; set; }
            public Views views { get; set; }
            public bool is_favorite { get; set; }
            public int edited { get; set; }
        }

        public class Group
        {
            public int id { get; set; }
            public string name { get; set; }
            public string screen_name { get; set; }
            public int is_closed { get; set; }
            public string type { get; set; }
            public int is_admin { get; set; }
            public int is_member { get; set; }
            public int is_advertiser { get; set; }
            public string photo_50 { get; set; }
            public string photo_100 { get; set; }
            public string photo_200 { get; set; }
        }

        public int count { get; set; }
        public List<Item> items { get; set; }
        public List<object> profiles { get; set; }
        public List<Group> groups { get; set; }
    }
}
