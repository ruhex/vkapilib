﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public interface IVkApi
    {
        string GenerateUrl(string methodName);
        Dictionary<string, string> Params { get; set; }
    }
}
