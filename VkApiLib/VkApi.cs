﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class VkApi : IVkApi
    {
        readonly private string _token;
        readonly private string _version;

        private static VkApi _instance;

        public Dictionary<string, string> Params { get; set; }

        public static VkApi GetInstance(string token, string version)
        {
            if (_instance == null)
                _instance = new VkApi(token, version);
            return _instance;
        }

        private VkApi(string token, string version)
        {
            Params = new Dictionary<string, string>();
            _token = token;
            _version = version;
        }

        public string GenerateUrl(string methodName)
        {
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> tmp in Params)
            {
                sb.Append($"{tmp.Key}={tmp.Value}&");
            }

            return $"https://api.vk.com/method/{methodName}?{sb.ToString()}access_token={_token}&v={_version}";
        }


    }
}
