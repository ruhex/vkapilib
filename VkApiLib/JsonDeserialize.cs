﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class JsonDeserialize<T> : IJsonDeserialize<T>
    {
        public T obj { get; set; }

        public JsonDeserialize(string json)
        {
            JsonConvert.DeserializeObject<Response<T>>(json);
        }

        
    }
}
