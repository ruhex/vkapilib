﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class Response<T> : IResponse<T>
    {
        public T response { get; set; }
    }
}
