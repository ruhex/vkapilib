﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public interface IResponse<T>
    {
        T response { get; set; }
    }
}
