﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class UploadServer
    {
        public string upload_url { get; set; }
        public int album_id { get; set; }
        public int user_id { get; set; }
    }
}
