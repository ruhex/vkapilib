﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public class Photo
    {
        public static List<Photo> Photos = new List<Photo>();
        public class Size
        {
            public string type { get; set; }
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public int id { get; set; }
        public int album_id { get; set; }
        public int owner_id { get; set; }
        public int user_id { get; set; }
        public List<Size> sizes { get; set; }
        public string text { get; set; }
        public int date { get; set; }
        public string access_key { get; set; }
        public int? post_id { get; set; }

        // For  Photos > photos.getUploadServer 
        #region 
        public int server { get; set; }
        public string photos_list { get; set; }
        public int aid { get; set; }
        public string hash { get; set; }
        public int gid { get; set; }
        #endregion

        // for IO file read
        public string Path { get; set; }
        public string Name { get; set; }
        public byte[] Bytes { get; set; }

        public Photo() { }
        public Photo(string path, string name)
        {
            Path = path;
            Name = name;
        }

        static public string GetSize(Photo img)
        {
            foreach (Size _tmp in img.sizes)
            {
                if (_tmp.type == "w")
                    return _tmp.url;
            }
            foreach (Size _tmp in img.sizes)
            {
                if (_tmp.type == "z")
                    return _tmp.url;
            }
            foreach (Size _tmp in img.sizes)
            {
                if (_tmp.type == "y")
                    return _tmp.url;
            }
            foreach (Size _tmp in img.sizes)
            {
                if (_tmp.type == "r")
                    return _tmp.url;
            }
            foreach (Size _tmp in img.sizes)
            {
                if (_tmp.type == "x")
                    return _tmp.url;
            }

            return null;
        }

        static public string GetUploadServer()
        {
            return null;
        }

    }
}
