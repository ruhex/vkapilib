﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkApiLib
{
    public interface IJsonDeserialize<T>
    {
        T obj {get;set;}
    }
}
